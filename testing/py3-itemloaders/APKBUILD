# Maintainer:
pkgname=py3-itemloaders
pkgver=1.1.0
pkgrel=2
pkgdesc="Library to populate items using XPath and CSS with a convenient API"
url="https://github.com/scrapy/itemloaders"
arch="noarch"
license="BSD-3-Clause"
depends="
	py3-itemadapter
	py3-jmespath
	py3-parsel
	py3-w3lib
	python3
	"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/scrapy/itemloaders/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/itemloaders-$pkgver"

build() {
	gpep517 build-wheel --wheel-dir .dist --output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest \
	--ignore tests/test_utils_python.py
	# https://github.com/scrapy/itemloaders/issues/76
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
}

sha512sums="
4a52e75405409124a3c1f7e7079fedf36e34a76877b04d754006743a2f403602de9b463e1f338bead3a68a0c1cf32f2a50b4cbe4cc57a10539ec529a93b81607  py3-itemloaders-1.1.0.tar.gz
"
